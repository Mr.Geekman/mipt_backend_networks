#!/bin/bash

mkfifo 2way
while true
do
    ncat -l localhost 8080 0<2way | tee >(ncat wiki.cs.hse.ru 80 1>2way) >(ncat localhost 8081)
done

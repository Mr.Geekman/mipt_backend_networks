#!/bin/bash

# clean before usage
rm /logs/requests.log
rm /logs/responses.log

mkfifo 2way
while true
do
    ncat -l localhost 8081 0<2way | tee -a /logs/requests.log | ncat localhost 8082 | tee -a /logs/responses.log 1>2way 
done

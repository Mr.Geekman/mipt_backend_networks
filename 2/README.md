# Задача 2. Анализ трафика.

## Условие

Вам предоставляется Docker-образ, в котором запускается бинарный файл. Требуется описать, что делает эта программа.

Dockerfile:
```Dockerfile
FROM ubuntu:latest
RUN apt -y update && apt-get -y install curl
RUN curl --output prog "http://85.93.88.189/some/prog" && chmod +x ./prog
CMD /prog
```

*Указание*: используйте Wireshark.

## Решение

Анализируем файл `dump.pcapng`.

Что мы видим:
1. Запрос DNS на `httpbin.org`
2. Обмен запросами между `192.168.0.103` и `3.209.99.235`.
3. Сначала они здороваются друг с другом и посылают информацию о том, как будут общаться под TLS.
4. Расшировываем трафик, для этого пользуемся настройкой переменной окружения согласно [документации](https://wiki.wireshark.org/TLS#using-the-pre-master-secret)
5. Стримы
   1. SETTINGS: служебное
   2. WINDOW_UPDATE: служебное
   3. Magic: служебное
   4. HEADERS: заголовки
   5. DATA: данные
6. DATA, HEADERS
   1. 
   HEADERS: 
   ```json
   {
       ":method": "POST",
       ":path": "/post",
       ":scheme": "https",
       ":authority": "httpbin.org",
       "user-agent": "user-agent: Mozilla/5.0",
       "accept": "*/*",
       "content-length": "159",
       "content-type": "application/x-www-form-urlencoded"
   }
   ``` 
   DATA: Мы отправляем серверу HTML-форму
   ```json
   {
       "custname": "edu",
       "custtel": "+79991234567",
       "custemail": "someone@hse.ru",
       "size": "large",
       "topping": "mushroom",
       "delivery": "11:00",
       "comments": "Moscow, 11, Pokrovsky boulevard. " 
   }
   ```
   2. В ответ получаем 
      HEADERS: 
   ```json
   {
       ":status": "200 OK",
       "date": "Mon, 07 Mar 2022 09:46:52 GMT", 
       "content-length": "644",
       "content-type": "application/json",
       "server": "gunicorn/19.9.0",
       "access-control-allow-origin": "*",
       "access-control-allow-credentials": "true" 
   }
   ``` 
   DATA: получаем json
   ```json
    {
    "args": {}, 
    "data": "", 
    "files": {}, 
    "form": {
        "comments": "Moscow,\r\n11, Pokrovsky boulevard. ", 
        "custemail": "someone@hse.ru", 
        "custname": "edu", 
        "custtel": "+79991234567", 
        "delivery": "11:00", 
        "size": "large", 
        "topping": "mushroom"
    }, 
    "headers": {
        "Accept": "*/*", 
        "Content-Length": "159", 
        "Content-Type": "application/x-www-form-urlencoded", 
        "Host": "httpbin.org", 
        "User-Agent": "user-agent: Mozilla/5.0", 
        "X-Amzn-Trace-Id": "Root=1-6225d48c-6656ada15405a5c871f7b083"
    }, 
    "json": null, 
    "origin": "93.175.28.49", 
    "url": "https://httpbin.org/post"
    }
   ```

from http import HTTPStatus


class HTTPException(Exception):
    def __init__(self, code: int, message: str):
        self.code = code
        self.message = message
        message = f"{HTTPStatus(code).phrase}: {message}"
        super().__init__(message)


class ClosedTCPConnection(Exception):
    def __init__(self):
        super().__init__("The TCP connection is closed")

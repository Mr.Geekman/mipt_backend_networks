import pathlib
import time
from http import HTTPStatus
from socket import AF_INET
from socket import SO_REUSEADDR
from socket import SOCK_STREAM
from socket import SOL_SOCKET
from socket import socket
from socket import timeout

from loguru import logger

from simple_http_server.exception import ClosedTCPConnection
from simple_http_server.exception import HTTPException
from simple_http_server.method import HTTPMethod
from simple_http_server.request import Request
from simple_http_server.response import make_response
from simple_http_server.split_buffer import SplitBuffer

HOST = "0.0.0.0"
PORT = 8080
CHUNK_SIZE = 1024
KEEP_ALIVE_TIMEOUT = 60
KEEP_ALIVE_SLEEP_TIMEOUT = 1.0
TIMEOUT_TIME = 0.5


def get_raw_request(client_sock) -> bytes:
    """Get raw request from the socket.

    There can be a few requests, done by pipelining.
    We can catch an empty request in two ways:
    1. Connection is closed, in this case we should send a signal about closing the connection on our side.
    2. We caught a timeout. In this case we can't close the connection because of keep-alive handling.

    :param client_sock: socket

    :return: raw request bytes
    """
    chunks = []
    while True:
        try:
            chunk = client_sock.recv(CHUNK_SIZE)
            if len(chunk) == 0:
                raise ClosedTCPConnection()
            chunks.append(chunk)
        except timeout:
            break
    raw_request = b"".join(chunks)
    return raw_request


def keep_alive_time_elapsed(start_pending):
    return (
        start_pending is not None
        and (time.time() - start_pending) >= KEEP_ALIVE_TIMEOUT
    )


def process_request(request) -> bytes:
    """Process the request according to our task.

    We should find the file and calculate the sum of integers inside of it.

    :param request: request info

    :return: ready to send response
    """
    if request.method != HTTPMethod.GET:
        raise HTTPException(
            code=HTTPStatus.METHOD_NOT_ALLOWED, message="Only GET method is available"
        )

    path = pathlib.Path(request.path)
    if path.exists():
        try:
            with path.open("r") as inf:
                contents = inf.read()
                elements = contents.split()
                result = sum(map(int, elements))
                return make_response(code=HTTPStatus.OK, message=str(result))
        except Exception as e:
            raise HTTPException(
                code=HTTPStatus.BAD_REQUEST,
                message="File doesn't satisfy a format: only integer numbers separated by newlines or spaces are allowed",
            )
    else:
        raise HTTPException(code=HTTPStatus.NOT_FOUND, message="Path doesn't exist")


def main():
    with socket(AF_INET, SOCK_STREAM) as sock:
        sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        sock.bind((HOST, PORT))
        sock.listen(1)
        logger.info(f"Starting the server at: http://{HOST}:{PORT}")
        # process many clients
        while True:
            # process one client
            client_sock, client_addr = sock.accept()
            logger.info(f"New client: {client_addr}")
            client_sock.settimeout(TIMEOUT_TIME)
            with client_sock:
                # process many requests per client in keep-alive mode
                buffer = SplitBuffer()
                is_closed = False
                start_pending = None
                while (not is_closed) and (not keep_alive_time_elapsed(start_pending)):
                    try:
                        logger.info("Try getting request")
                        raw_request = get_raw_request(client_sock)
                    except ClosedTCPConnection:
                        logger.info("Connection was closed by the client")
                        break

                    # we caught a timeout, but connection isn't closed
                    if len(raw_request) == 0:
                        # start counting time before closing the connection
                        if start_pending is None:
                            start_pending = time.time()
                        time.sleep(KEEP_ALIVE_SLEEP_TIMEOUT)
                    else:
                        start_pending = None

                    buffer.feed_data(raw_request)

                    while len(buffer):
                        keep_alive = False
                        try:
                            logger.info("Start processing the request")
                            request = Request(buffer)
                            logger.info(f"Request info: {request}")
                            keep_alive = request.keep_alive
                            logger.info("Generating the response")
                            response = process_request(request)
                        except HTTPException as e:
                            logger.error(f"HTTPException occurred: {e}")
                            response = make_response(code=e.code, message=e.message)
                        except Exception as e:
                            logger.error("Internal exception occurred: {e}")
                            response = make_response(
                                code=HTTPStatus.INTERNAL_SERVER_ERROR, message=str(e)
                            )

                        logger.info("Sending the response")
                        client_sock.sendall(response)
                        if not keep_alive:
                            # don't forget to break outer cycle
                            is_closed = True
                            break


if __name__ == "__main__":
    main()

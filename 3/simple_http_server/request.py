from urllib.parse import unquote
from http import HTTPStatus

from simple_http_server.exception import HTTPException
from simple_http_server.method import HTTPMethod
from simple_http_server.split_buffer import SplitBuffer


class Request:
    """Request for this task."""

    def __init__(self, buffer: SplitBuffer):
        """Init the request.

        :param buffer: buffer with data to construct request from
        """
        self.line_separator = b"\r\n"
        self.headers_separator = ": "
        self.buffer = buffer
        self.parse_start_line()
        self.parse_headers()
        self.parse_data()

    def __str__(self):
        return f"{self.method} {self.path}; Headers: {self.headers}; Data: {self.data}"

    def parse_start_line(self):
        """Parse start line from the request."""
        start_line = self.buffer.pop(separator=self.line_separator)
        try:
            elements = start_line.decode().strip().split()
            if len(elements) != 3:
                raise HTTPException(
                    code=HTTPStatus.BAD_REQUEST, message="Start line isn't correct"
                )

            method, path, protocol = elements

            # verify given fields
            self.method = self._verify_method(method)
            self.path = self._verify_path(path)
            self.protocol = self._verify_protocol(protocol)

        except UnicodeDecodeError:
            raise HTTPException(
                code=HTTPStatus.BAD_REQUEST, message="Start line isn't correct"
            )

    def parse_headers(self):
        """Parse headers from the request."""
        header_line = self.buffer.pop(separator=self.line_separator)
        self.headers = {}
        while header_line:
            try:
                name, value = (
                    header_line.decode()
                    .strip()
                    .split(self.headers_separator, maxsplit=1)
                )
                self.headers[name.lower()] = value
                header_line = self.buffer.pop(separator=self.line_separator)

            except UnicodeDecodeError:
                raise HTTPException(
                    code=HTTPStatus.BAD_REQUEST, message="Headers line isn't correct"
                )

    def parse_data(self):
        """Parse data from the request."""
        content_length = int(self.headers.get("content-length", 0))
        if content_length:
            self.data = self.buffer.flush(content_length)
        else:
            self.data = b""

    @property
    def keep_alive(self) -> bool:
        connection_option = self.headers.get("Connection", "keep-alive")
        return connection_option == "keep-alive"

    @staticmethod
    def _verify_method(method: str) -> HTTPMethod:
        """Verify the method in first line of HTTP request."""
        try:
            return HTTPMethod(method)
        except ValueError:
            raise HTTPException(
                code=HTTPStatus.BAD_REQUEST, message="Incorrect method name"
            )

    @staticmethod
    def _verify_path(url: str) -> str:
        """Verify the path in first line of HTTP request."""
        path = unquote(url)
        return path

    @staticmethod
    def _verify_protocol(protocol: str) -> str:
        """Verify the protocol in first line of HTTP request."""
        if protocol != "HTTP/1.1":
            raise HTTPException(
                code=HTTPStatus.BAD_REQUEST, message="Incorrect protocol"
            )
        return protocol

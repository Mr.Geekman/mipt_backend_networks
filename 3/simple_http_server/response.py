from http import HTTPStatus


def make_response(code: int, message: str) -> bytes:
    """Make the response.

    Example: b"HTTP/1.1 200 OK\n\nHello World"

    :param code: HTTP code.
    :param message: text with result.

    :return: bytes with response
    """
    message_len = len(message.encode())
    result_utf = f"HTTP/1.1 {code} {HTTPStatus(code).phrase}\nContent-length: {message_len}\n\n{message}"
    result_bytes = result_utf.encode()
    return result_bytes

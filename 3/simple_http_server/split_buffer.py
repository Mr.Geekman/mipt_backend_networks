class SplitBuffer:
    """Class for extraction of bytes by separator."""

    def __init__(self):
        self.data = b""

    def __len__(self):
        return len(self.data)

    def feed_data(self, data: bytes):
        self.data += data

    def pop(self, separator: bytes):
        first, *rest = self.data.split(separator, maxsplit=1)
        # there is no split
        if not rest:
            return None
        else:
            self.data = separator.join(rest)
            return first

    def flush(self, num_bytes):
        temp = self.data[:num_bytes]
        self.data = self.data[num_bytes:]
        return temp

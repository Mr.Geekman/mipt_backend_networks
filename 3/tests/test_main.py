import pathlib
import subprocess
import time
from http import HTTPStatus
from http import client
from urllib.parse import quote

import pytest

from simple_http_server.main import HOST
from simple_http_server.main import PORT

MAIN_PATH = (
    pathlib.Path(__file__)
    .parent.absolute()
    .parent.joinpath("simple_http_server")
    .joinpath("main.py")
)
DATA_PATH = (
    pathlib.Path(__file__).parent.absolute().parent.joinpath("example").joinpath("data")
)
process = None


def setup_module(module):
    """Setup test module by starting the server."""
    global process
    process = subprocess.Popen(["python", MAIN_PATH])
    time.sleep(1.0)


def teardown_module(module):
    """Tear down test module by killing the server."""
    global process
    if process is not None:
        process.kill()


@pytest.fixture
def example_url() -> str:
    """Example url for a file in a correct format."""
    path = DATA_PATH.joinpath("2.txt")
    url = quote(str(path))
    return url


def test_get_one(example_url):
    """Test main function on one get request."""
    conn = client.HTTPConnection(host=HOST, port=PORT)
    conn.request(method="GET", url=example_url)
    response = conn.getresponse().read()
    conn.close()

    assert int(response) == 4


def test_get_two_sequential_sessions(example_url):
    """Test main function on two get requests in two different sessions."""
    conn = client.HTTPConnection(host=HOST, port=PORT)
    conn.request(method="GET", url=example_url)
    response_1 = conn.getresponse().read()
    conn.close()

    conn = client.HTTPConnection(host=HOST, port=PORT)
    conn.request(method="GET", url=example_url)
    response_2 = conn.getresponse().read()
    conn.close()

    assert int(response_1) == 4
    assert int(response_2) == 4


def test_get_two_sequential_requests_one_session(example_url):
    """Test main function on two get requests in one session."""
    conn = client.HTTPConnection(host=HOST, port=PORT)
    conn.request(method="GET", url=example_url)
    response_1 = conn.getresponse().read()
    conn.request(method="GET", url=example_url)
    response_2 = conn.getresponse().read()
    conn.close()

    assert int(response_1) == 4
    assert int(response_2) == 4


def test_get_two_pipelining(example_url):
    """Test main function on two get requests in pipelining.

    Taken from: https://github.com/urllib3/urllib3/issues/52#issuecomment-109756116
    """
    conn = client.HTTPConnection(host=HOST, port=PORT)
    conn.request(method="GET", url=example_url)
    response_1_template = conn.response_class(conn.sock, method=conn._method)
    conn._HTTPConnection__state = "Idle"
    conn.request(method="GET", url=example_url)
    response_2_template = conn.response_class(conn.sock, method=conn._method)
    conn._HTTPConnection__state = "Idle"
    response_1_template.begin()
    response_2_template.begin()
    response_1 = response_1_template.read()
    response_2 = response_2_template.read()
    conn.close()

    assert int(response_1) == 4
    assert int(response_2) == 4


def test_not_found():
    """Test main function on file that can't be found."""
    conn = client.HTTPConnection(host=HOST, port=PORT)
    conn.request(method="GET", url="/non/existent/path")
    response_code = conn.getresponse().getcode()
    conn.close()

    assert response_code == HTTPStatus.NOT_FOUND


def test_not_allowed(example_url):
    """Test main function on POST request, when only GET is allowed."""
    conn = client.HTTPConnection(host=HOST, port=PORT)
    conn.request(method="POST", url=example_url, body="{}")
    response_code = conn.getresponse().getcode()
    conn.close()

    assert response_code == HTTPStatus.METHOD_NOT_ALLOWED
